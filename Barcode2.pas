unit Barcode2;
{
Printer scaling functions

Copyright 1998-2000 Andreas Schmidt


use this functions only between
printer.BeginDoc and printer.EndDoc

}


interface

uses Printers;



function ConvertMmToPixelsX(const Value:Double;APrinter:TPrinter):Integer;
function ConvertMmToPixelsY(const Value:Double;APrinter:TPrinter):Integer;
function ConvertInchToPixelsX(const Value:Double;APrinter:TPrinter):Integer;
function ConvertInchToPixelsY(const Value:Double;APrinter:TPrinter):Integer;



implementation

uses Windows;

const mmPerInch = 25.4;


function GetPrinterRes(const pobj: TPrinter; Horz: Boolean): integer;
var
   Index: Integer;
begin
   if Horz then
      Index:=LOGPIXELSX
   else
      Index:=LOGPIXELSY;
   Result:=GetDeviceCaps(pobj.Handle, Index);
end;


function ConvertMMtoPixelsX(const Value:Double;APrinter:TPrinter):Integer;
begin
   Result := Round(Value*GetPrinterRes(APrinter, True) / mmPerInch);
end;

function ConvertMMtoPixelsY(const Value:Double;APrinter:TPrinter):Integer;
begin
   Result := Round(Value*GetPrinterRes(APrinter, False) / mmPerInch);
end;

function ConvertInchtoPixelsX(const Value:Double;APrinter:TPrinter):Integer;
begin
   Result := Round(Value*GetPrinterRes(APrinter, True));
end;

function ConvertInchtoPixelsY(const Value:Double;APrinter:TPrinter):Integer;
begin
   Result := Round(Value*GetPrinterRes(APrinter, False));
end;


end.
