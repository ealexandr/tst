unit BlobToJpegHlp;

interface

uses
  UstAPI, SDKROHlp, BlobToJpeg;

type
  TBlobToJpegHlp = class(TSDKROHlp)
    constructor Create; overload;
    destructor Destroy; override;
  	function Load: Boolean; overload;
	  function Load(parent: LongWord): Boolean; overload;
    function Create(logger: ILogger; error: Cardinal): Boolean; overload;
  end;

const
  BLOBTOJPEG_DLL_NAME = 'BlobToJpeg.dll';

var
  BlobToJpgHlp: TBlobToJpegHlp;

implementation

constructor TBlobToJpegHlp.Create;
begin
  inherited;
end;

destructor TBlobToJpegHlp.Destroy;
begin
end;

function TBlobToJpegHlp.Load: Boolean;
begin
	Result := LoadByPath(BLOBTOJPEG_DLL_NAME);
end;

function TBlobToJpegHlp.Load(parent: LongWord): Boolean;
begin
	Result := LoadByParent(parent, BLOBTOJPEG_DLL_NAME);
end;

function TBlobToJpegHlp.Create(logger: ILogger; error: Cardinal): Boolean;
begin
	Result := CreateRO(Addr(BLOBTOJPEG_API_ID), BLOBTOJPEG_API_VERSION, logger, error);
end;

end.
