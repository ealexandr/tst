from book
unit MainUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Grids, Vcl.DBGrids,
  Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TMainForm = class(TForm)
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    CheckBox1: TCheckBox;
    LoginEdit: TEdit;
    PwdEdit: TEdit;
    FIOEdit: TEdit;
    AddButton: TButton;
    EditButton: TButton;
    CanselButton: TButton;
    ChangePwdButton: TButton;
    DelButton: TButton;
    Button6: TButton;
    procedure Button6Click(Sender: TObject);
    procedure AddButtonClick(Sender: TObject);
    procedure CanselButtonClick(Sender: TObject);
    procedure DelButtonClick(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const [Ref] Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure EditButtonClick(Sender: TObject);
    procedure ChangePwdButtonClick(Sender: TObject);
  private
    procedure DisableButton;
    procedure EnableButton;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation
 uses DMUnit;
{$R *.dfm}

procedure TMainForm.AddButtonClick(Sender: TObject);
var adm : String;
begin
 if AddButton.Caption = '��������' then begin
  DisableButton;
  AddButton.Enabled := True;
  AddButton.Caption := '���������';
  LoginEdit.Enabled := True;
  PwdEdit.Enabled := True;
  FIOEdit.Enabled := True;
  CheckBox1.Enabled := True;
  LoginEdit.SetFocus;
 end
 else begin
  TRY
   DM.OQuery.Close;
   if CheckBox1.Checked then
    adm := 'T'
   else
    adm := 'F';
   DM.OQuery.SQL.Text := 'CREATE USER '+LoginEdit.Text +
                         ' IDENTIFIED BY '+PwdEdit.Text+
                         ' TEMPORARY TABLESPACE TEMP '+
                         ' ACCOUNT UNLOCK';
   DM.OQuery.Execute;
   DM.OQuery.SQL.Text := 'GRANT CREATE SESSION TO '+LoginEdit.Text;
   DM.OQuery.Execute;
   DM.OQuery.SQL.Text := 'GRANT PERMITION TO '+LoginEdit.Text+' WITH ADMIN OPTION';
   DM.OQuery.Execute;
   if adm='T' then begin
    DM.OQuery.SQL.Text := 'GRANT PERMITION_ADMIN TO '+LoginEdit.Text+' WITH ADMIN OPTION';
    DM.OQuery.Execute;
   end;

   DM.OQuery.SQL.Text := 'ALTER USER AUTODOR DEFAULT ROLE ALL';
   DM.OQuery.Execute;
   DM.OQuery.SQL.Text := 'insert into A_USERS(U_LOGIN,FULL_NAME,  ADMIN)'+
                         ' values('+#39+LoginEdit.Text+#39+','+#39+ FIOEdit.Text+#39+','+#39+adm+#39+')';
   DM.OQuery.Execute;
   AddButton.Caption := '��������';
   EnableButton;
   DM.OSession.Commit;
   DM.UsersODS.Refresh;
  EXCEPT
   on e : Exception do begin
    ShowMessage(e.Message);
    DM.OSession.Rollback;
   end;
  END;

 end;
end;

procedure TMainForm.Button6Click(Sender: TObject);
begin
 Close;
end;

procedure TMainForm.CanselButtonClick(Sender: TObject);
begin
 EnableButton;
end;

procedure TMainForm.ChangePwdButtonClick(Sender: TObject);
begin
 if ChangePwdButton.Caption = '������� ������' then begin
  DisableButton;
  ChangePwdButton.Enabled := True;
  ChangePwdButton.Caption := '���������';
  PwdEdit.Enabled := True;
  LoginEdit.Text := DM.UsersODS.FieldByName('U_LOGIN').AsString;
  FIOEdit.Text := DM.UsersODS.FieldByName('FULL_NAME').AsString;
  CheckBox1.Checked := Boolean(DM.UsersODS.FieldByName('ADMIN').AsString='T');
  PwdEdit.SetFocus;
 end
 else begin
  TRY
   DM.OQuery.Close;
   DM.OQuery.SQL.Text := 'ALTER USER '+ANSIUpperCase(LoginEdit.Text) +' IDENTIFIED BY '+PwdEdit.Text;
   DM.OQuery.Execute;
   EditButton.Caption := '������� ������';
   EnableButton;
  EXCEPT
   on e : Exception do begin
    ShowMessage(e.Message);
    DM.OSession.Rollback;
   end;
  END;

 end;
end;

procedure TMainForm.DBGrid1DrawColumnCell(Sender: TObject;
  const [Ref] Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
if (DM.UsersODS.FieldByName('KILL_DATE').AsString <>'') then begin
   DBGrid1.Canvas.Font.Color := clRed;
   DBGrid1.Canvas.TextOut(Rect.Left+1,Rect.Top+1,DM.UsersODS.FieldByName(Column.FieldName).AsString);
  end
end;

procedure TMainForm.DelButtonClick(Sender: TObject);
begin
if MessageDLG('������� ������������ '+DM.UsersODS.FieldByName('FULL_NAME').AsString+ '?',mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
 TRY
   DM.OQuery.SQL.Text := 'DROP USER '+DM.UsersODS.FieldByName('U_LOGIN').AsString+' CASCADE';
   DM.OQuery.Execute;

   DM.OQuery.SQL.Text := 'UPDATE A_USERS SET KILL_DATE=sysdate,KILLER= (select U_ID from A_USERS where U_LOGIN = USER) where U_ID='+DM.UsersODS.FieldByName('U_ID').AsString;
   DM.OQuery.Execute;
   DM.UsersODS.RefreshRecord;
   DM.OSession.Commit;
 EXCEPT
  on e : Exception do begin
    ShowMessage(e.Message);
  end;
 END;
end;
end;

procedure TMainForm.DisableButton;
begin
 AddButton.Enabled := False;
 EditButton.Enabled := False;
 ChangePwdButton.Enabled := False;
 CanselButton.Enabled := True;
 DelButton.Enabled := False;
end;

procedure TMainForm.EditButtonClick(Sender: TObject);
var adm : String;
begin
 if EditButton.Caption = '�������������' then begin
  DisableButton;
  EditButton.Enabled := True;
  EditButton.Caption := '���������';
  FIOEdit.Enabled := True;
  CheckBox1.Enabled := True;
  LoginEdit.Text := DM.UsersODS.FieldByName('U_LOGIN').AsString;
  FIOEdit.Text := DM.UsersODS.FieldByName('FULL_NAME').AsString;
  CheckBox1.Checked := Boolean(DM.UsersODS.FieldByName('ADMIN').AsString='T');
  FIOEdit.SetFocus;
 end
 else begin
  TRY
   DM.OQuery.Close;
   if CheckBox1.Checked then
    adm := 'T'
   else
    adm := 'F';
   if adm='T' then begin
    DM.OQuery.SQL.Text := 'GRANT "PERMITION_ADMIN" TO '+LoginEdit.Text+' WITH ADMIN OPTION';
    DM.OQuery.Execute;
   end
   else begin
    DM.OQuery.SQL.Text := 'REVOKE "PERMITION_ADMIN" FROM '+LoginEdit.Text;
    DM.OQuery.Execute;
   end;


   DM.OQuery.SQL.Text := 'update A_USERS set FULL_NAME='+#39+FIOEdit.Text+#39+',  ADMIN='+#39+adm+#39+
                         ' where U_LOGIN ='+#39+LoginEdit.Text+#39;
   DM.OQuery.Execute;
   EditButton.Caption := '�������������';
   EnableButton;
   DM.OSession.Commit;
   DM.UsersODS.Refresh;
  EXCEPT
   on e : Exception do begin
    ShowMessage(e.Message);
    DM.OSession.Rollback;
   end;
  END;

 end;
end;

procedure TMainForm.EnableButton;
begin
 AddButton.Enabled := True;
 AddButton.Caption := '��������';
 EditButton.Enabled := True;
 EditButton.Caption := '�������������';
 ChangePwdButton.Enabled := True;
 ChangePwdButton.Caption := '������� ������';
 CanselButton.Enabled := False;
 DelButton.Enabled := True;
 LoginEdit.Text := '';
 PwdEdit.Text := '';
 FIOEdit.Text := '';
 LoginEdit.Enabled := False;
 PwdEdit.Enabled := False;
 FIOEdit.Enabled := False;
 CheckBox1.Enabled := False;
end;
end.
