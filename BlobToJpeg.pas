unit BlobToJpeg;

interface

uses
  UstApi, DataReader;

type
  //---------------------------------------------------------------------------
  // ��������� ����������
  //---------------------------------------------------------------------------
  IBlobToJpegConverter = class(IBase)
  	function GetDataReader(buffer: Pointer; size: Cardinal): IDataReader; virtual; stdcall; abstract;

	  // ��������� ��� ������ ������
  	// DataReader: IDataReader;
    // DataReader := GetDataReader(buffer: Pointer, size: Cardinal);

	  // ������� �����������
    // Width, Height, Quality, size: Cardinal;
    // Color: Boolean;
    // buffer: Pointer;
  	// Width := DataReader.ReadIntS('MainImage', 'Width', 0);
  	// Height := DataReader.ReadIntS('MainImage', 'Height', 0);
  	// Color := DataReader.ReadBoolS('MainImage', 'Color', False);
  	// Quality := DataReader.ReadIntS('MainImage', 'Quality', 0);
  	// size := 0;
  	// buffer = NULL;
  	// DataReader.ReadBLOBS('MainImage', 'Image', buffer, size);

  	// ���� ������
  	// rect: PRECT;
    // rect := nil;
  	// DataReader.ReadBLOBS('MainImage', 'Rect', Pointer(rect), size);

	  // ����� ������������ ��������� ���������
  	// time: PSystemTime;
    // time := nil;
  	// DataReader.ReadBLOBS('MainImage', 'TLTime', Pointer(time), size);

	  // ��������� ��������� (0-��� ������, 1-���������, 2-���������, 3-���������)
    // tl_status: Cardinal;
  	// tl_status := DataReader.ReadInt('MainImage', 'TLStatus', 0);

	  // ����������� ������
    // Width, Height, Quality, size: Cardinal;
    // Color: Boolean;
    // buffer: Pointer;
  	// Width := DataReader.ReadIntS('PlateImage', 'Width', 0);
	  // Height := DataReader.ReadIntS('PlateImage', 'Height', 0);
  	// Color := DataReader.ReadBoolS('PlateImage', 'Color', False);
  	// Quality := DataReader.ReadIntS('PlateImage', 'Quality', 0);
  	// size := 0;
  	// buffer := NULL;
	  // DataReader.ReadBLOBS('PlateImage', 'Image', buffer, size);

  	// ���������� ���. �����������
    // count: Cardinal;
  	// count := DataReader.ReadInt('AddImagesCount', 0); 0 <= index < count

	  // ���. �����������
    // Width, Height, Quality, size: Cardinal;
    // Color: Boolean;
    // buffer: Pointer;
  	// Width := DataReader.ReadIntS('AddImage_index', 'Width', 0);
	  // Height := DataReader.ReadIntS('AddImage_index', 'Height', 0);
  	// Color := DataReader.ReadBoolS('AddImage_index', 'Color', False);
	  // Quality := DataReader.ReadIntS('AddImage_index', 'Quality', 0);
  	// size := 0;
	  // buffer := nil;
  	// DataReader.ReadBLOBS('AddImage_(index)', 'Image', buffer, size);

	  // ���� ������ (������, ����� �� ����)
    // rect: PRECT;
  	// rect := nil;
  	// DataReader.ReadBLOBS('AddImage_(index)', 'Rect', Pointer(rect), size);

  	// ���� ������ ((X) - �� 0 �� 3)
  	// p: POINT;
  	// p.x := DataReader.ReadIntS('AddImage_(index)', 'RecAreaX_(X)', rect, size);
  	// p.y := DataReader.ReadIntS('AddImage_(index)', 'RecAreaY_(X)', rect, size);

  	// ���� ����� ((X) - �� 0 �� 1)
  	// p: POINT;
	  // p.x := DataReader.ReadIntS('AddImage_(index)', 'StopLineX_(X)', rect, size);
  	// p.y := DataReader.ReadIntS('AddImage_(index)', 'StopLineY_(X)', rect, size);

  	// ���� ���������
  	// rect: PRECT;
    // rect := nil;
  	// DataReader.ReadBLOBS('AddImage_(index)', 'TLArea', Pointer(rect), size);

	  // ����� �����
  	// sys_time: PSystemTime
    // sys_time := nil;
  	// DataReader.ReadBLOBS('AddImage_(index)', 'SysTime', Pointer(sys_time), size);

	  // ����� ������������ ��������� ���������
  	// sys_time: PSystemTime
    // sys_time := nil;
  	// DataReader.ReadBLOBS('AddImage_index', 'TLTime', Pointer(time), size);

  	// ��������� ��������� (0-��� ������, 1-���������, 2-���������, 3-���������)
    // tl_status: Cardinal;
  	// tl_status := DataReader.ReadInt('AddImage_index', 'TLStatus', 0);

    // camera: PChar;
    // channel: Cardinal;
	  // camera := DataReader.ReadStrS('AddImage_(index)', 'Camera', '');
  	// channel := DataReader.ReadIntS('AddImage_(index)', 'Channel', 0);

    // radar_name, radar_id, radar_sertificate, radar_sertificate_date: PChar;
	  // radar_name(complex_name) := DataReader.ReadStrS('Radar', 'Name', dsc_tree.ReadStr('Name', ''));
  	// radar_id(complex_id) := DataReader.ReadStrS('Radar', 'ID', dsc_tree.ReadStr('RadarID', ''));
	  // radar_sertificate(complex_sertificate) := DataReader.ReadStrS('Radar', 'Certificate', dsc_tree.ReadStr('RadarSert', ''));
  	// radar_sertificate_date(complex_sertificate_date) := DataReader.ReadStrS('Radar', 'CertificateDate', DateToStr(TDateTime(dsc_tree.ReadFloat64('RadarDate', 0))));

    // video_count: Cardinal;
    // image_host, host, camera, channel: PChar;
	  // video_count := tree.WriteInt('VideoCount', video_count); 0 <= index < video_count
  	// image_host := tree.ReadStrS(section_i, 'ImageHost', dsc_tree.ReadStr('ImageHost', ')); ������ �������� �����
  	// host := tree.ReadStrS(section_i, 'Host', dsc_tree.ReadStr('Host', '')); ��� ���������� (�������� �������)
  	// camera := tree.ReadStrS(section_i, 'Camera', dsc_tree.ReadStr('Camera', ')); ��� ������ (�������� �������)
  	// channel := tree.ReadIntS(section_i, 'Channel', dsc_tree.ReadInt('Channel', 0)); ����� ������ (�������� �������)
  end;

  //---------------------------------------------------------------------------
  // ��������� ��������� ������� ����������
  //---------------------------------------------------------------------------
  IBlobToJpeg = class(IBase)
    // ������� �������� ����������
    // ���������:
    // converter - ��������� �� ��������� ���������� ����������
    // ������������ ��������: ���
    procedure CreateConverter(converter: IBlobToJpegConverter); virtual; stdcall; abstract;
  end;

const
  BLOBTOJPEG_DLL_NAME: String = 'BlobToJpeg.dll';
  //---------------------------------------------------------------------------
  // {FDB2AAFD-7A27-4084-8DBD-7C3A0DF3EB11}
  //---------------------------------------------------------------------------
  BLOBTOJPEG_API_ID: TGUID = '{FDB2AAFD-7A27-4084-8DBD-7C3A0DF3EB11}';
  //---------------------------------------------------------------------------
  BLOBTOJPEG_API_LOCAL_VERSION = 2;
  //---------------------------------------------------------------------------
  BLOBTOJPEG_API_VERSION = UInt64(UST_API_LOCAL_VERSION) shl 32 or BLOBTOJPEG_API_LOCAL_VERSION;

implementation

end.
