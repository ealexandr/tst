unit DMUnit;

interface

uses
  System.SysUtils, System.Classes, Data.DB, OracleData, Oracle,
  OracleLogon,Vcl.Forms,Vcl.Dialogs;

type
  TDM = class(TDataModule)
    OSession: TOracleSession;
    OLogon: TOracleLogon;
    UsersODS: TOracleDataSet;
    UsersDS: TDataSource;
    OQuery: TOracleQuery;
    UsersODSU_ID: TFloatField;
    UsersODSU_LOGIN: TStringField;
    UsersODSFULL_NAME: TStringField;
    UsersODSCREATOR: TFloatField;
    UsersODSCREATE_DATE: TDateTimeField;
    UsersODSKILLER: TFloatField;
    UsersODSKILL_DATE: TDateTimeField;
    UsersODSADMIN: TStringField;
    UsersODSCREATOR_FULL_NAME: TStringField;
    UsersODSKILLER_FULL_NAME: TStringField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM: TDM;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TDM.DataModuleCreate(Sender: TObject);
var s : String;
i,j : Integer;
begin
 TRY

    ltLogonTitle   := '���� � �������: �������. ����������� ����������. ';    // Title of Logon dialog
    ltPasswordTitle:= '��������� ������'; // Title of Change password dialog
    ltConfirmTitle := '�����������';         // Title of password confirmation dialog
    ltUsername     := '������������';
    ltPassword     := '������';
    ltDatabase     := '���� ������';
    ltConnectAs    := 'Connect as';
    ltNewPassword  := '����� ������';
    ltOldPassword  := '������ ������';
    ltVerify       := '��������';
    ltVerifyFail   := '�������� �� ��������';
    ltChangePassword:= '������ �������� ������ ������?';
    ltExpired      := '���� �������� ������ �����';
    ltOKButton     := 'OK';
    ltCancelButton := '������';
    ltHistoryHint  := 'Logon history';

     OLogon.Execute;

    if not OSession.Connected then
     begin
      Application.Terminate;
      Exit;
     end;
    OQuery.SQL.Text := 'select ADMIN from A_USERS where UPPER(U_LOGIN)='+#39+ANSIUpperCase(OSession.LogonUsername)+#39;
    OQuery.Execute;
    if (OQuery.RowCount>0 ) and (OQuery.FieldAsString(0)<>'T') then begin
     ShowMessage('������������ �� �������� ���������������');
     Application.Terminate;
    end;
   EXCEPT
    on E: Exception do
     begin
     ShowMessage(e.Message);
      Application.Terminate;
     end;
   END;
//   WGC_ODS.SQL.Text := 'select r.*,'+#39+','+#39+'||r.WGC_ID||'+#39+','+#39+' WGC_COMMA from p_ref_wgc r from p_ref_wgc r';
   TRY
     UsersODS.Open;
//    MainForm.Activate;
   EXCEPT
    on E: Exception do
     begin
      ShowMessage('������ ��� �������� �������'+#13+#10+e.Message);
      Application.Terminate;
     end;
   END;
end;

end.
